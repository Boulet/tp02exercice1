﻿using System;

namespace Exercice1Console
{
    public class Program
    {
        public static int Exercice1(int facteur_gauche, int facteur_droite)
        {
            return facteur_gauche + facteur_droite;
        }
        static void Main(string[] args)
        {
          char first_letter, last_letter;
            string phrase;
            int longueur_chaine;

            Console.WriteLine("Veuillez entrer une phrase.");
            phrase=Console.ReadLine();


            first_letter=phrase[0];
            longueur_chaine=phrase.Length;
            last_letter=phrase[longueur_chaine-1];


            if (first_letter>= 'A' && first_letter <= 'Z')
            {
                Console.WriteLine("Cette phrase commence par une majuscule");
            }

            if (last_letter == '.')
            {
                Console.WriteLine("Cette phrase se termine par un point");
            }


            Console.WriteLine("Hello World!");
            Console.WriteLine("{0}",first_letter);
            Console.WriteLine("{0}",longueur_chaine);
            Console.WriteLine("{0}",last_letter);
        }
    }
}