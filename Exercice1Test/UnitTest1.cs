using System;
using Xunit;

using Exercice1Console;

namespace Exercice1Test
{
    public class OperationTest
    {
        [Fact]
        public void TestExercice1()     //je n'ai pas compris ce qui devait être testé dans l'exercice 1
        {
            Assert.Equal(4, Program.Exercice1(2, 2));
        }
    }
}